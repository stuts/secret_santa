#!/usr/bin/env ruby

require 'yaml'
require 'gmail'

contactfile = 'contacts.yaml'
contacts = YAML.load_file(contactfile)

=begin

PSEUDO CODE FOR SELECTION

For each person
  Select a random person from contacts
  Keep choosing a new person until:
    - Person isn't themself

  TODO Currently there's the possibility of the program
  getting stuck if there's an odd number and everyone
  but the last iterated contact is left as a recipient.

=end
recipient_list = contacts.keys

contacts.each do |name, details|
  recipient = recipient_list.sample
  until name != recipient
    recipient = recipient_list.sample
  end
  recipient_list.delete(recipient)
  details['recipient'] = recipient
end


# Login to gmail
puts "Gmail Login: "
username = gets.chomp
puts "Password: "
password = STDIN.noecho(&:gets).chomp
gmail = gmail.connect(username, password)

# Send email
contacts.each do |name, details|
  puts "#{name} has #{details['recipient']}"
end
